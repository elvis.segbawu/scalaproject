package expected

sealed trait LList[+A]{
  def isEmpty: Boolean
  def length: Int
  def stringRepresentation: String
  override def toString: String = "[" + stringRepresentation + "]"
  def ::[B>:A](newElement:B) : LList[B]
  def ++[B>:A](otherList: LList[B]) : LList[B]
  def map[B >: A](f: (B) => B) : LList[B]
  def flatMap[B >: A](f: (B) => B) : LList[B]
  def filter[B >: A](f: B => Boolean) : LList[B]
}

case class Empty[A]() extends LList[A]{
  override def isEmpty: Boolean = true

  override def length: Int = 0

  override def stringRepresentation: String = ""

  override def ::[B >: A](newElement: B): LList[B] = NonEmpty(newElement, this)

  override def ++[B >: A](otherList: LList[B]): LList[B] = otherList

  override def map[B >: A](f: (B) => B): LList[B] = this

  override def flatMap[B >: A](f: B => B): LList[B] = ???

  override def filter[B >: A](f: B => Boolean) : LList[B] = this
}

case class NonEmpty[+A](head : A, tail : LList[A]) extends LList[A]{
  override def isEmpty: Boolean = false

  override def length: Int = 1 + tail.length

  override def stringRepresentation: String = if (tail.isEmpty) "" + head else head + ", " + tail.stringRepresentation

  override def ::[B >: A](newElement: B): LList[B] = NonEmpty(newElement, this)

  override def ++[B >: A](otherList: LList[B]): LList[B] = NonEmpty(head, tail ++ otherList)

  override def map[B >: A](f: (B) => B): LList[B] = NonEmpty(f(head), tail.map(f))

  override def flatMap[B >: A](f: B => B): LList[B] = NonEmpty(f(head), tail.flatMap(f))

  override def filter[B >: A](f: B => Boolean) : LList[B] = {
    if (f(head)) NonEmpty(head, tail.filter(f))
    else tail.filter(f)
  }
}

object LList{
  def apply[A](items : A*):LList[A] = if(items.isEmpty) Empty[A] else NonEmpty(items.head, apply(items.tail : _*))
}
