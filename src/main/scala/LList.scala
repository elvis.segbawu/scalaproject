/*
import scala.annotation.tailrec

sealed trait LList2[+A] {
  def isEmpty: Boolean
  def ::[B >: A](element: B): LList2[B]
}

case object Empty extends LList2[Nothing] {
  override def isEmpty:Boolean = true

  override def ::[B >: Nothing](element: B): LList2[B] = NonEmpty(element, Empty)
}

case class NonEmpty[A](hd: A, tl: LList2[A]) extends LList2[A] {
  override def isEmpty: Boolean = false
  override def ::[B >: A](element: B): LList2[B] = NonEmpty(element, this)
}

sealed trait ListTrait[+A] {
  
  case class Node(data:A, var next: ListTrait[A]#Node)

  def isEmpty: Boolean

  def length: Int

  def `::`[B >: A](elem: B): LList[B]

  def `++`[B >: A](ls1: LList[B]): LList[B]

}

class LList[A](private val _head:ListTrait[A]#Node) extends ListTrait[A] {

  val head = _head

  override def isEmpty: Boolean = _head == null

  override def length: Int = {
    @tailrec
    def lengthAcc(node: ListTrait[A]#Node, acc: Int): Int = {
      if (node == null) acc else lengthAcc(node.next, acc + 1)
    }
    lengthAcc(_head, 0)
  }

  override def ::[B >: A](elem: B): LList[B] = {
    new LList[B](new ListTrait[B]#Node(elem, this._head))
  }

  def tail(): ListTrait[A]#Node ={
    @tailrec
    def findTail(node: ListTrait[A]#Node): ListTrait[A]#Node = {
      if (node.next == null) node
      else findTail(node.next)
    }
    findTail(_head)
  }

  override def ++[B >: A](ls1: LList[B]): LList[B] = {
    tail.next = ls1._head
    new LList[B](_head)
  }

  override def toString: String = {
    @tailrec
    def appendStr(node: ListTrait[A]#Node, str_acc: String): String = {
      if (node == null) "[" + str_acc + "]"
      else {
        appendStr(node.next, s"${if (str_acc.nonEmpty) s"${str_acc}, " else ""}${node.data}")
      }
    }
    appendStr(_head, "")
  }
}

object X extends App {
  val ls = new LList[String](new ListTrait[String]#Node("asdsad", null))
  println(ls)
}

*/
