/*
import scala.annotation.tailrec

sealed trait AbstractLList[A] {

  var _head: AbstractLList[A]#Node

  case class Node(data:A, var next: AbstractLList[A]#Node)

  def isEmpty:Boolean

  def length:Int

//  def `::` (elem:A): AbstractLList[A]
  def `::` (elem:A): Unit

  def  `++`( ls1:AbstractLList[A] ) : Unit

  override def toString: String = super.toString

}

class LList[A] extends AbstractLList[A] {



  override var _head: AbstractLList[A]#Node = _

  override def isEmpty: Boolean = _head == null

  override def length: Int = {
    @tailrec
    def lengthAcc(node: AbstractLList[A]#Node, acc:Int): Int ={
      if (node == null) acc
      else lengthAcc(node.next, acc+1)
    }
    lengthAcc(_head, 0)
  }

  override def `::`(elem: A): Unit = {
//    val new_head:Node = new Node(elem, _head)
    _head = new Node(elem, _head)
//    println(_head)
  }

  override def `++`(ls1:AbstractLList[A]):Unit = {
    @tailrec
    def concat(node: AbstractLList[A]#Node):Unit = {
      if (node.next == null) node.next = ls1._head
      else concat(node.next)
    }
    concat(_head)
  }

  override def toString: String = {
    @tailrec
    def appendStr(node:AbstractLList[A]#Node, str_acc:String): String ={
      if (node == null) "[" + str_acc + "]"
      else {
        appendStr(node.next, s"${if (!str_acc.isEmpty)  s"${str_acc}, " else ""}${node.data}")
      }
    }
    appendStr(_head, "")
  }
}

object LList {
  def apply[A] (args: A*): LList[A] = {
    @tailrec
    def init(ls:LList[A], args:Seq[A]): LList[A] ={
      if (args.isEmpty) ls
      else {
        ls.`::`(args.last)
        init(ls, args.slice(0, args.length-1))
      }
    }
    init(new LList[A](), args)

//    @tailrec
//    def init(currentHead: [A]#Node, idx:Int): AbstractLList[A]#Node ={
//      if (idx < 0) currentHead
//      else {
//        init( new AbstractLList[A]#Node(args(idx)), idx - 1)
//      }
//    }
//    new LList[A](init(null, args.length - 1))
  }
}

object X extends App {
  val ls = LList[String]("asd","asdasdasd")
  println(ls)
}
*/
