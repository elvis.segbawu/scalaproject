package expected

import munit.FunSuite

class LinkedListTest extends FunSuite {

  test("Filtering list for even numbers should return a list of even numbers") {
    val myList = LList(1, 2, 3, 4)
    assertEquals(LList(2, 4), myList.filter(x => x % 2 == 0))
  }

  test("Filtering list for odd numbers should return a list of odd numbers") {
    val myList = LList(1, 2, 3, 4)
    assertEquals(LList(1, 3), myList.filter(x => x % 2 != 0))
  }

}